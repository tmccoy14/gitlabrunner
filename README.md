# GitLabRunner

## Getting started

To run Gitlab CI/CD pipelines, users can use Gitlab provided runners called shared runners or else create their own runners.

- [ ] [Install Gitlab Runner on GKE](https://blog.searce.com/install-gitlab-runner-on-gke-with-application-deployment-e7370403a62f)
